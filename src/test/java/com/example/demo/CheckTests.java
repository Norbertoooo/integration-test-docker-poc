package com.example.demo;

import org.junit.jupiter.api.Test;
import org.springframework.web.client.RestTemplate;

import static org.assertj.core.api.Assertions.assertThat;


public class CheckTests {

    @Test
    public void homeTest() {
        RestTemplate resTemplate = new RestTemplate();
        String body = resTemplate.getForObject("http://172.18.0.2:8080/test", String.class);
        assertThat(body).isEqualTo("Spring is here!");
    }
}
